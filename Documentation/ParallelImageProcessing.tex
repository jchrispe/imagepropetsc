\documentclass[11pt]{article}
\usepackage{geometry} \geometry{hmargin={1in,1in},vmargin={1in,1in}}
\usepackage{amsmath,amsfonts}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage{wrapfig}
\usepackage[dvips]{graphics}
\usepackage[dvips]{graphicx}
\usepackage{listings}
%\usepackage{floatflt}
\usepackage{wrapfig}
%\author{John Chrispell}
%\date{\today}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% Some Custome Colors to Choose  %%%%%%%%%%%%%%%%%%
\usepackage[usenames]{color}
\usepackage{colortbl}
\definecolor{ClemsonPurple}{rgb}{.2, 0, .4}
\definecolor{ClemsonOrange}{rgb}{1,.388,0}
\definecolor{ClemsonBlue}{rgb}{.011,.298,.51764}
\definecolor{LightBlue}{rgb}{.95,.99,1}
\definecolor{Silver}{rgb}{.4,.4,.4}
\newenvironment{snuglist}
       {
       \begin{list}{}
         {
           \setlength{\leftmargin}{0.35in}
           \setlength{\topsep}{0.1in}
	   \setlength{\parskip}{0in}
	   \setlength{\partopsep}{0in}
           \setlength{\itemsep}{0.035in}
	   \setlength{\parsep}{0in}
         }
       }
       {\end{list}}
\newcommand{\bX}{{\bf X}}
\newcommand{\bx}{{\bf x}}
\begin{document}
\lstset{language=C, basicstyle=\ttfamily\small, keywordstyle=\color{blue}\bfseries,
stringstyle=\color{ClemsonOrange}, showstringspaces=false,morecomment=[l][\color{ClemsonOrange}]{//},morecomment=[s][\color{ClemsonPurple}]{/*}{*/}}
\title{Introduction to PETSc and \\ \vskip.1in Parallel Image Processing}
\lstset{
  rangeprefix=/*\ Document\ Code:--\ ,
  rangesuffix=\ --*/,
  includerangemarker=false
}
\author{John Chrispell}
\date{Indiana University of Pennsylvania \\ Department of Mathematics}
\date{August 2015}
\maketitle
\begin{abstract}
The PETSc framework for parallel computation is demonstrated using an example from image processing. PETSc is a toolkit accessible from many programming languages. Here a basic introduction to the PETSc library routines available for distributed computation are used to transform a color image to a grayscale version. Users of this code framework could easily extend the module to do other image processing routines, including edge detection, filtering, and anti-aliasing. This introduction to PETSc will serve as a gateway for users to become familiar with the code framework and as an accessing point using PETSc for solving other scientific modeling problems. \footnote{This material is based upon work supported by the National Science Foundation under Grant Number 1258604. Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation.}
\end{abstract}

\noindent{\bf Pre-requisite knowledge required:} The readers of this module are assumed to be familiar with the basics of programming in C, and with the use of Makefiles.

\section*{Overview}
This curriculum module is designed to introduce students to the PETSc framework for parallel image processing. Using image processing as an application context, the objectives, goals, and outcomes for a student include:
\begin{itemize}
\item Introduce the user to a parallel computational framework that is easily extended to many different problems.
\item Introduce a basic form of digital image storage and manipulation.
\item Demonstrate a simple form of parallel image processing that is easily extendable to other forms of processing and image manipulation.
\end{itemize}
After using this code module a user will have:
\begin{itemize}
\item Learned basic parallel programming techniques.
\item Learned the difference between bookkeeping in an application-based context and in a code-oriented context.
\item Become familiar with image processing techniques.
\item Gained experience with the PETSc parallel coding framework and become able to use it for other applications beyond just image processing.
\end{itemize}
The expected duration to complete the materials is one to two classes, longer if library installation is included as part the discussion. Installation of the PETSc library is necessary in order to run the provided application code. The installation could be done prior to starting the following activities or could be included as part of the discussion. The background given to students on image processing could take as little as 15 minutes or could fill a semester long course.

Extensions of the PETSc framework to other scientific applications could also be used to augment or extend the module. There are many application codes and examples that are present on the PETSc website. Note PETSc can be configured to use different parallel frameworks under the hood, including standard MPI, GPUs through CUDA or OpenCL, and hybrid MPI-GPU parallelism.

\section{Introduction}
For larger and larger problems, the computational methods required have moved toward parallel and high performance computing. With an eye on high performance computing, computational libraries are becoming an important part of a programmer's toolkit. These libraries allow for some of the message processing and lower-level routines to be circumvented when examining large scale problems. This document will serve as an introduction to using some of the routines in the PETSc libraries. PETSc stands for the Portable Extendable Toolkit for Scientific Computation and is a collection of data structures and routines designed for parallel solutions of scientific applications. Information about PETSc may be found on the project's website~\cite{petsc-web-page} and in the PETSc users manual~\cite{petsc-user-ref}. The website includes a list of scientific applications that use the toolkit under the hood as well as information about the different types of parallel computing supported.

\begin{figure}[t!]
\centering
\begin{eqnarray*}
\includegraphics[width=2.3in,height=2.7in,angle=0]{YellowStone.jpg}
& {\text{Transforms to: }}\atop{\Longrightarrow} &
\includegraphics[width=2.3in,height=2.7in,angle=0]{YellowStoneBW.jpg}
\end{eqnarray*}
\caption{A color image can be transformed to a black-and-white image using an average of the red, green, and blue pixel values in the original image.   \label{ColorToBWexample}}
\end{figure}

The potential uses for a computational toolkit like PETSc are limitless. The libraries are especially useful when finding the numerical solution to partial differential equations. In order to focus our exploration of the PETSc framework and parallel computation in a broader setting, let's take some inspiration from Ansel Adams and consider the transformation of a color landscape into a black-and-white copy of the original image. The original image and product after transformation are shown in Figure~\ref{ColorToBWexample}. The color to black-and-white transformation is accomplished using the image processing code included in the Appendix. The example program is parallel and once compiled may be run using the standard parallel execution:
\begin{quote}
\begin{verbatim}
mpirun -np <processors> ./ImagePro
\end{verbatim}
\end{quote}
The Appendix gives a brief description of the sample program's directory structure and how it is compiled. The following sections will showcase the basics of how the image transformation is accomplished and PETSc programming, with Section~\ref{image:basics} describing the representation and storage of an image. Section~\ref{petsc:basics} details some of the PETSc routines and data structures used in parallel processing. A collection of possible projects is given in Section~\ref{projects}. An assessment rubric is given in Section~\ref{assessment}, and the Appendix contains listings of the computational program used to process a color image into a black-and-white version.

\section{Working with Images}
\label{image:basics}

A portable pixel map or PPM is a simple way of describing an image. The pixel map file contains all the information about a given image and can be thought of as a header and pixel data. The header depicts the image type (given by a magic number), image size (width and height in pixels) and image brightness. The pixel data is an array of values starting in the upper left hand corner of the image that progresses across and down. A typical PPM header is depicted in Figure~\ref{typicalheader}.
\begin{center}
\begin{figure}[h!]
\centering
\begin{tabular}{ll}
{\tt P5} & : Magic Number \\
{\tt \# Comments} & : Any number of comment lines allowed starting with `\#' \\
{\tt 300 400} & : Image width and height in pixels\\
{\tt 255 } & : Max image brightness value\\
\end{tabular}
\caption{\label{typicalheader}Layout of typical PPM header. The magic number may be followed by comments or directly by the image dimensions. }
\end{figure}
\end{center}
The magic number in Figure~\ref{typicalheader} is given as `P5' and denotes a grayscale image. A magic number of `P6' would denote a color image. The magic number value and the image size allows for the appropriate storage space for the pixel data to be determined. Note that color images need a red, a green, and a blue component for each pixel, whereas grayscale images only need a single component. Thus, the image data section of the PPM file will contain:
\begin{eqnarray*}
\left(\text{image width} \times \text{image height}\times \text{3}\right) & & \text{bytes of data for a color image} \\
\left(\text{image width} \times \text{image height}\right) & & \text{bytes of data for a grayscale image.}
\end{eqnarray*}
The PPM file format may be viewed using a variety of software packages including XV or the GNU image manipulation program GIMP \cite{xv, gimp}. PPM files can be opened in ImageMagick in BCCD by using the {\verb'display' command, e.g.:
\begin{quote}
\verb'display YellowStone.ppm &'
\end{quote}
Other variations on this file format are available and store the pixel data in ASCII format; however, here the focus will be on using the image data stored in binary, with an unsigned char used to depict pixel values between 0 and 255.

\section{PETSc Basics}
\label{petsc:basics}
PETSc can be downloaded and installed using the directions on the package's website \cite{petsc-web-page}. Once the library is installed, a user should be sure that the PETSc paths are set. This can be done by modifying the {\it .bashrc} or {\it .profile} file in the user's home directory to include the following lines:
\begin{quote}
\begin{verbatim}
export PETSC_DIR=/Path/To/Library/petsc-XXX
export PETSC_ARCH=littleFe-c-debug
\end{verbatim}
\end{quote}
As of the writing of this documentation, the author used:

\begin{verbatim}
--configModules=PETSc.Configure --optionsModule=config.compilerOptions 
--download-hypre --with-mpi-dir=/bccd/software/openmpi/1.8.5/Linux/BCCD/x86_64
\end{verbatim}

Note the version of PETSc installed may vary. Once the \verb'PETSC_DIR' variable has been set, PETSc routines may be added to code by adding the appropriate include in the project Makefiles:
\begin{quote}
\begin{lstlisting}
# PETSC 3.6 Directories
include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules
\end{lstlisting}
or
\begin{lstlisting}
# PETSC 3.3-p2
include ${PETSC_DIR}/conf/variables
include ${PETSC_DIR}/conf/rules
include ${PETSC_DIR}/conf/test
\end{lstlisting}
\end{quote}
An example of this is included in the Makefiles associated with the image processing project included in the Appendix (see Sections \ref{makefile:A} and \ref{makefile:B}).  In addition to augmenting the project Makefiles, including
\begin{quote}
\begin{verbatim}
#include "petscdmda.h"
\end{verbatim}
\end{quote}
in the {\it Library.h} file insures access to the appropriate function signatures needed for using PETSc distributed array data structures. Instructions for installing PETSc on LittleFe  with BCCD can be found on the BCCD wiki \cite{bccdwiki}:
\begin{center}
\url{http://bccd.net/wiki/index.php/PETSc}
\end{center}

\subsection*{PETSc Speedup}
When installing the PETSc framework, the install script gives the user an option of testing the speedup across the machine on which the PETSc library is being installed. With a 6-node, 12-core LittleFe, Figure~\ref{speedup} shows the bandwidth speedup achieved when multiple processors are used. The speedup stagnates after 11 processors, showing the limitations of the LittleFe hardware by maxing out near the total number of cores available.
\begin{figure}[h!]
\includegraphics[scale=0.5]{PETScPepper.pdf}
\caption{MPI speedup for multiple processors. \label{speedup}}
\end{figure}

\subsection*{Program Structure and Options}
The parallel implementation of a PETSc program is taken care of using a call to \verb'PetscInitialize()'. The function call to \verb'PetscInitialize()' does several things including calling \verb'MPI_Init()' and setting up a PETSc database. The PETSc database is a collection of options that can be altered at run-time for different PETSc routines and data structures. The basic framework of a PETSc program is given by:
\begin{quote}
\begin{lstlisting}
    PetscInitialize(&argc,&args,PETSC_NULL,PETSC_NULL);
    /* Main Program Code */
    PetscFinalize();
\end{lstlisting}
\end{quote}
In the parallel image processing code contained in the Appendix, the call to \verb'PetscInitialize()' occurs inside the \verb'readimage_init()' function and will set PETSc database options by reading the file {\it image.params} by default. Users may alter the file used for the PETSc database by setting the an option flag after the executable on the command line as follows:
\begin{quote}
\begin{verbatim}
mpirun -np <processors> ./ImagePro -parameter.file <filename>
\end{verbatim}
\end{quote}

The parallel image processing code has several options that may be set at run-time. The PETSc database option {\it -imageIn} is used to set the file name for the image to be processed. Using the following PETSc function call will set this parameter in the program:
\begin{quote}
\lstinputlisting[language=C, linerange=optionfile\-start-optionfile\-end]{../Library/InitializeImage.c}
\end{quote}
Here the string after the flag {\it -imageIn} is set into the {\tt imageIn} parameter in the {\tt FileInfo} data structure. For more examples of how to use PETSc to set code parameters, see the \verb'readimage_init()' function in the {\it initializeImage.c} file.



\subsection*{The Distributed Array}
The key player in the parallel implementation of image processing will be distributing storage of images across multiple processors. PETSc will allow a user to work with an application ordering and maintains a mapping to a distributed ordering of data arrays. A standard application ordering for an image 4 pixels wide and 3 pixels high would be as follows:
\begin{equation}
\begin{array}{cccc}
0 & 1 & 2 & 3 \\
4 & 5 & 6 & 7 \\
8 & 9 & 10 & 11
\end{array}
\label{standard}
\end{equation}
The standard application ordering (\ref{standard}) would be the same as the PETSc distributed ordering when working with only one processor. If the image processing is to be done using multiple processors, the application ordering would stay the same; however, the PETSc distributed ordering of the array may change. The default PETSc ordering for the 4 by 3 distributed array on two processors is:
\begin{equation}
\begin{array}{cc|cc}
0 & 1 & 6 & 7 \\
2 & 3 & 8 & 9 \\
4 & 5 & 10 & 11
\end{array}
\label{petsc2proc}
\end{equation}
Note the image pixel data is then stored with the right and left hand sides of the image stored on different processors. A mapping is required to know where pixel data should be stored in the PETSc distributed array when thinking about it using the standard application ordering. If the image processing to be done requires knowledge of pixel neighbors, ghost pixel values are required at the edges of the pixel domain stored on each processor. An example of a single pixel ghost storage for the small 12 pixel example is given by:
\begin{equation}
{{\begin{array}{cc|c}
0 & 1 & 6 \\
2 & 3 & 8 \\
4 & 5 & 10
\end{array}}\atop{\text{Rank 0}}}
\hskip.5in
{{\begin{array}{c|cc}
1 & 6 & 7 \\
3 & 8 & 9 \\
5 & 10 & 11
\end{array}}\atop{\text{Rank 1}}}
\label{simpleghosting}
\end{equation}

To create a distributed array with PETSc, first create an object that manages the distribution across multiple processors with the function call signature:
\begin{quote}
\begin{lstlisting}
DMDACreate2d(MPI_Comm        , // MPI communicator
             DMDABoundaryType, // Type of Boundary Ghosting in x
             DMDABoundaryType, // Type of Boundary Ghosting in y
             DMDAStencilType , // Distributed array stenciling
             PetscInt        , // Global Dimensions in X direction
             PetscInt        , // Global Dimensions in Y direction
             PetscInt        , // Number of Processors in x direction
             PetscInt        , // Number of Processors in y direction
             PetscInt        , // Degrees of freedom at each node
             PetscInt        , // Stencil width
             const PetscInt[], // Nodes in each cell in x or NULL
             const PetscInt[], // Nodes in each cell in y or NULL
             DM                // Resulting PETSc distributed array
             );
\end{lstlisting}
\end{quote}
See the \verb'Image_init()' function in the '{\it InitializeImage.c}' listing in the Appendix for use of \\ \verb'DMDACreate2d()' in context. The PETSc user manual \cite{petsc-user-ref} contains a detailed description of this function and additional examples. A key parameter in this function call is the degrees of freedom at each of the nodes. This allows for pixels to be indexed in a row and column fashion with the degree of freedom used to hold the red, green, and blue pixel values at the given index.

An application ordering corresponding to distributed array allows for mappings between application and distributed array orderings. To create the application ordering the function call signature is:
\begin{quote}
\begin{lstlisting}
DMDAGetAO(DM, // DM is the PETSc distributed management data structure
           AO // AO is the PETSc array ordering data structure
          );
\end{lstlisting}
\end{quote}
A programmer may move between the PETSc distributed array ordering and the standard application ordering using a PETSc mapping function:
\begin{quote}
\begin{lstlisting}
AOApplicationToPetsc(AO,       // PETSc application ordering context
                 PetscInt ,  // number of values to be mapped
                 PetscInt[]  // values to be replaced by mapped value
                 );
\end{lstlisting}
\end{quote}
Once the distributed array management object has been created it can be used to create a distributed array or vector. The PETSc signature for creating a distributed array would be as follows:
\begin{quote}
\begin{lstlisting}
DMCreateGlobalVector(DM, // PETSc distributed management structure
                     Vec // PETSc distributed vector
                     );
\end{lstlisting}
\end{quote}
In the parallel image processing code included as an example the following loop is used for setting pixel values from a file into a PETSc distributed array vector. Single values are read from the file and placed appropriately. Note the \verb'VecSetValue()' function could set more than a single value at a time improving the efficiency of the program. Values set into the global distributed array may be cached, so after using \verb'VecSetValue()', a call to the \verb'VecAssemblyBegin()' and \verb'VecAssemblyEnd()' functions must be done. These calls ensure the proper message passing has occurred and stored values on the correct processor. Code that is independent of the vector being assembled could be placed between these function calls giving a place for potential performance enhancement.
\begin{quote}
\lstinputlisting[language=C, linerange=DMDACreate\-start-DMDACreate\-end]{../Library/InitializeImage.c}
\end{quote}
The provided example code for image processing creates two PETSc distributed array vectors: one for the original color image using three degrees of freedom at each node, and a second using only a single degree of freedom for the grayscale image. The details of the distributed storage can be viewed using a PETSc viewer.

\subsection*{Using the Distributed Array}
Once a distributed array has been assembled and is living potentially on multiple processors, some work is required to gain access to the values stored within the array. Communications will allow for the image processing (or other data manipulations of interest) to be accomplished in a parallel manner. \\

\noindent {\bf Example:} Looking at a very small ppm image file:
\begin{quote}
\begin{verbatim}
P6 4 3 255
!!~!!~!!~!!~!!~!!~~~!~~!~~!~~!~~!~~!
\end{verbatim}
\end{quote}
We see this is a color image with 12 pixels, the first six being \verb'!!~' and the next six being \verb'~~!'. The image has 4 colums and 3 rows and can be stored across multiple processors in many ways. To see how PETSc stores the image using the distributed array structure, the image processing code is compiled with the following debug flag set in the Library Makefile.
\begin{quote}
\begin{verbatim}
DEBUGFLAGS = -DDBG_DAVIEWERS
\end{verbatim}
\end{quote}
Note that this should only be done when small image files are used, as the visualization will be unreadable if an image of too many pixels is used. Figure~{\ref{distarray}} shows the default distribution using 1, 2, 3 and 4 processors as well as the application ordering of the pixels and associated ghost pixel numbering for the array. The reader should be aware that the PETSc default when viewing an application ordering of a distributed array is to start in the lower left corner of the array, in contrast to the default ordering for pixels in a ppm image that start in the upper right corner.   

\begin{figure}[b!]
\begin{tabular}{cc|cc}
\includegraphics[width=2.8in]{Dis1Proc.png} & \hspace{0.1in} & \hspace{0.1in} &
\includegraphics[width=2.8in]{Dis2Proc.png} \\
-n 1 & & & -n 2 \\ \hline
\includegraphics[width=2.8in]{Dis3Proc.png} & \hspace{0.1in} & \hspace{0.1in} &
\includegraphics[width=2.8in]{Dis4Proc.png} \\
-n 3 & & & -n 4
\end{tabular}
\caption{\label{distarray}Distributed storage of the image pixels across processors. Red segments denote the pixels stored on individual processors, and the numbering shows the application ordering of the pixels and the appropriate ghost numbering.}
\end{figure}

\subsection*{Color to Black-and-White Image Processing}
\label{ctobwimageprocessing}
Manipulation of images on a pixel by pixel basis is be done by altering the red (r), green (g), and blue (b) pixel values in organized ways. A simple method that may be used to convert a color image to a black-and-white image involves averaging the rgb pixel values and outputting a single black-and-white (bw) pixel value for the corresponding pixel. In the provided image processing code, the averaging occurs in the {\it ManipulateImage.c} file. Looping over each rgb pixel in the image, a bw pixel value is found by:
\begin{quote}
\lstinputlisting[language=C, linerange=color2bwporcess\-start-color2bwporcess\-end]{../Library/ManipulateImage.c}
\end{quote}
Other image processing techniques may be implemented using augmentations of this loop.

\subsection*{Performance}
\label{performance}
Testing the performance of the color to black-and-white image processing on the LittleFe cluster was done on the {\it YellowStone.ppm} image included with this curriculum module and shown in Figure~\ref{ColorToBWexample}. The base image is 2816 pixels by 2112 pixels in size. To showcase the speedup of the image processing portion of the code, timing calls are used in the main function located in \verb'BasicImage_main.c'. One timing keeps track of only the time spent completing the image processing, and a second time is kept to quantify the total wall clock time spent to complete the whole program.  Running the image processing using up to 12 processes yields the results shown in Figure~\ref{processtimes}. Note that as the number of processors is increased, the time spent completing the image processing decreases significantly. It should be noted that the time spent setting up the required data structures and communication dominate the overall computation time. To see a true advantage to parallel processing of the image, more complicated work (something more sophisticated than averaging three numbers on each pixel) is required, making a case for parallel image processing.
\begin{figure}[h!]
\includegraphics[width=3.15in]{ParallelMPIYellowstone.pdf}
\hspace{.2in}
\includegraphics[width=3.15in]{WallTimeMPIYellowstone.pdf}
\caption{\label{processtimes} Image processing and wall clock time computed when running the color to black-and-white image processing on the {\it YellowStone.ppm} image.}
\end{figure}

\section{Projects}
\label{projects}
There are many ways to extend and improve the image processing code presented here. Augmenting the code to improve efficiency can be done in several places. The following is a list of places for potential improvements and extensions. Comparisons can be made with the provided implementation to see how these alterations potentially improve the efficiency of the program.
\begin{itemize}
\item The `original image read' presented is done using only a single processor and could be done with each processor reading its own section of the file considered.
\item Change the multi-processor distributed array storage of images from being done with the PETSc default to being separated in a row oriented fashion. This can be accomplished by small changes to the \verb'DMDACreate2d()' function call.
\item Extend the provided code to set more than one value at a time by altering the function calls to \verb'VecSetValue()'.
\end{itemize}
Moving beyond the color to black-and-white image processing algorithm used here, a user could implement many different processing techniques. Suggestions for further image processing techniques include:
\begin{itemize}
\item Edge detection using a gradient or more advanced approach.
\item Blurring and sharpening techniques.
\item Manipulating the colors or changing the color of specific objects.
\item Transforming a black-and-white image to a color image.
\item Applying image filters.
\end{itemize}
This list is only limited by the reader's imagination, and students could search the literature for ideas that could extend the code presented in this module.

\section{Assessment Rubric}
\label{assessment}
This rubric gauges a students’ knowledge and experience after using these materials. Students should use a scale of (1-5) for each of the following concepts and ideas:
\begin{itemize}
\item Image representation
\item Image Processing
\item Parallel Algorithm Design 
\item External libraries for advanced programming
\item The PETSc Library
\item Using a Cluster
\item Scaling Parallel Code
\item The types of problems that appropriately scale on high performance computing machines. 
￼
\end{itemize}

\noindent In addition, students may be asked the following questions:
\begin{enumerate}
\item What was the most useful aspect of this module?
\item What was the least useful aspect?
\item What did you learn that you would be excited to know more about?
\item What aspects of the presented module were not useful or irrelevant?
\end{enumerate}

%\clearpage
\bibliography{alphabib}
\bibliographystyle{plain}

\clearpage
\section{Appendix}
\label{appendix}
Complete code listings for the Parallel Image Processing Code. The code may be compiled from the projects directory using the command:
\begin{quote}
\begin{verbatim}
make libs
\end{verbatim}
\end{quote}
The first command compiles the library files used and the later links the library files with the main image processing function file. Issuing the command below will clean up the directories and allow for easily debugging extensions of the code.
\begin{quote}
\begin{verbatim}
make cleanlibs
\end{verbatim}
\end{quote}

\subsection{Directory Structure}
The following is the directory structure for the image processing code listed from the `Top' down. The code could be easily extended by adding new types of manipulation into the Library and Projects directories.
\begin{quote}
\begin{verbatim}
Top/Documentation/ParallelImageProcessing.tex
Top/Documentation/YellowStone.jpg
Top/Documentation/YellowStoneBW.jpg
Top/Projects/BasicReader/Makefile
Top/Projects/BasicReader/BasicImage_main.c
Top/Projects/BasicReader/image.params
Top/Projects/BasicReader/YellowStone.ppm
Top/Projects/BasicReader/PlayImage2.ppm
Top/Library/Makefile
Top/Library/LibraryHeader.h
Top/Library/InitializeImage.c
Top/Library/ManipulateImage.c
Top/Library/TestMain.c
Top/Library/WriteImage.c
\end{verbatim}
\end{quote}

\lstset{basicstyle=\tiny}
\subsection{Makefile}
\label{makefile:A}
The following is a Makefile for an individual project. You can make the needed code library and then link to the current project using:
\begin{quote}
\verb'make libs'\\
\verb'make'
\end{quote}
\lstinputlisting[language=make]{../Projects/BasicReader/Makefile}

\subsection{BasicImage\textunderscore main.c}
\label{basicimagemain}
A basic project main file.
\lstinputlisting[language=C]{../Projects/BasicReader/BasicImage_main.c}

\subsection{image.params}
\label{image.params}
Images to be processed and the procedures to be implemented can be placed in a .params file for easy scripting.
\lstinputlisting[language=C]{../Projects/BasicReader/image.params}

\subsection{Makefile}
\label{makefile:B}
\lstinputlisting[language=C]{../Library/Makefile}

\subsection{LibraryHeader.h}
\label{LibraryHeader}
\lstinputlisting[language=C]{../Library/LibraryHeader.h}

\subsection{InitializeImage.c}
\label{InitializeImage}
\lstinputlisting[language=C]{../Library/InitializeImage.c}

\subsection{ManipulateImage.c}
\label{ManipulateImage}
\lstinputlisting[language=C]{../Library/ManipulateImage.c}

\subsection{WriteImage.c}
\label{WriteImage}
\lstinputlisting[language=C]{../Library/WriteImage.c}

\subsection{InitializeImage.c}
\lstinputlisting{../Library/InitializeImage.c}




\end{document}
