import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

fig, ax1 = plt.subplots()
plt.title('MPI Parallel Performance')
ax1.set_autoscaley_on(False)
    
x        = [1          , 2          , 3          , 4          , 5          , 6         , 7          , 8          , 9          , 10           , 11         , 12         ];
runtime  = [ 31.751531 , 18.999478  , 14.525365  , 12.556094  , 11.140114  , 10.629230 , 9.692329   , 9.771794   , 8.880678   ,  8.951679    , 8.391178   , 9.183786   ];
walltime = [ 134.758812, 149.356496 , 141.771398 , 140.442565 , 140.265894 , 144.598788, 142.501087 , 143.814918 , 140.725354 ,  145.062942  , 142.399914 , 143.968932 ];


ax1.set_xlabel('Number of MPI processes')
ax1.set_ylabel('Timing (s)')
test1, = ax1.plot(x, runtime  ,'b-o',label="test1")
plt.legend(["Parallel Run-Time"],loc=3)
ax1.set_xlim([1,12])
ax1.set_ylim([0,33])
ax1.set_ylabel("Timeing (s)")
plt.show()

with PdfPages('ParallelMPIYellowstone.pdf') as pdf:
    plt.rc('text', usetex=False)
    pdf.savefig(fig)  # or you can pass a Figure object to pdf.savefig
    # We can also set the file's metadata via the PdfPages object:
    d = pdf.infodict()
    d['Title']    = 'MPI Process Timing'
    d['Author']   = 'John Chrispell'
    d['Subject']  = 'MPI Timings for Yellowstone.ppm'
    d['Keywords'] = 'Parallel Image Processing PETSc'
    plt.close()


fig2, ax2 = plt.subplots()
plt.title('MPI Performance Wall-Time')
ax2.set_xlabel('Number of MPI processes')
ax2.set_ylabel('Timing (s)')
test2, = ax2.plot(x, walltime ,'r-o',label="test2")
plt.legend(["Wall Clock-Time"],loc=3)
ax2.set_ylim([0,150])
ax2.set_ylabel("Timing (s)")
plt.show()

with PdfPages('WallTimeMPIYellowstone.pdf') as pdf:
    plt.rc('text', usetex=False)
    pdf.savefig(fig2)  # or you can pass a Figure object to pdf.savefig
    # We can also set the file's metadata via the PdfPages object:
    d = pdf.infodict()
    d['Title']    = 'MPI Process Timeing'
    d['Author']   = 'John Chrispell'
    d['Subject']  = 'MPI Timings for Yellowstone.ppm'
    d['Keywords'] = 'Parallel Image Processing PETSc'
    plt.close()

