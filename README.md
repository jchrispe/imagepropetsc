# README #

This is a parallel image processing code. The current project directory has one application that will take a color .ppm image and convert it to a black and white copy of itself. 

The goal of this code if to let the user experiment with PETSc libraries using an image processing application.

* The code requires use of the PETSc libraries for scientific computing. 
* Current version of PETSc used through the code is: 3.6 Released June 9th 2015.  
* [PETSc](http://www.mcs.anl.gov/petsc/)

### How do I get set up? ###

* Install PETSc via directions on the PETSc website. 
* cd into the projects directory
* make libs
* make

### Contribution guidelines ###

* Please freely distribute and add to this code framework. 

### Who do I talk to? ###

* John Chrispell  john.chrispell@iup.edu