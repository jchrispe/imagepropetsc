#include "LibraryHeader.h"

void Image_Write(fileInfo_t *FileInfo, image_t *Image){

    #ifdef DBG_FUNCTIONCALL
        PetscPrintf(MPI_COMM_WORLD,"CALL Image_Write()\n");
        PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);
    #endif

    /** 
     * Function used to write the finished image to a file with name specified 
     * at runtime. Note that this only needs to be done on the head node.    
     */ 

    PetscViewer        viewer;                     /* Petsc Viewer used    */ 
    MPI_Comm           comm;                       /* Communincation       */ 
    char  NameOfFileToWrite[PETSC_MAX_PATH_LEN];   /* Out File name        */
    PetscInt           i, Index, Size;             /* index, image size    */
    VecScatter         scatterctx;                 /* Vec Scatter context  */ 
    Vec                singProcessorImage;         /* Full Velocity vec    */ 
    PetscScalar        Values[1];                  /* Current Char holder  */  

    /* ================================================================ */ 
    /* Create the file name to be used and open the file.               */    
    /* ================================================================ */   

    PetscObjectGetComm((PetscObject) Image->pixelOld, &comm);
    NameOfFileToWrite[0] = 0;     
    strcat(NameOfFileToWrite,FileInfo->saveBase[0]); 
    strcat(NameOfFileToWrite,FileInfo->imageOut[0]); 

    #ifdef DBG_BASIC
        PetscPrintf(PETSC_COMM_WORLD,"OUT FILE NAME: %s \n", NameOfFileToWrite); 
        PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);
    #endif

    PetscViewerASCIIOpen(comm,NameOfFileToWrite, &viewer);
 
    /* ================================================================ */ 
    /* Write the header of the file                                     */
    /* ================================================================ */ 

    if(Image->DegreesOfFreedomNew == 1){
        PetscViewerASCIIPrintf(viewer, "P5");
    } else {
        PetscViewerASCIIPrintf(viewer, "P6");
    }
        
    PetscViewerASCIIPrintf(viewer, " %d %d %d\n", Image->ColumnsNew, Image->RowsNew, Image->BrightnessNew);

    VecScatterCreateToAll(Image->pixelNew,&scatterctx,&singProcessorImage); 
    VecScatterBegin(scatterctx,Image->pixelNew,singProcessorImage,INSERT_VALUES,SCATTER_FORWARD);
    VecScatterEnd(scatterctx,Image->pixelNew,singProcessorImage,INSERT_VALUES,SCATTER_FORWARD);

    /* ================================================================ */ 
    /* Write the image                                                  */ 
    /* ================================================================ */ 
    
    Size = (Image->DegreesOfFreedomNew*Image->RowsNew*Image->ColumnsNew); 
    for(i = 0; i < Size ; i++){
        Index = i; 
        AOApplicationToPetsc(Image->ao_imageNew,1,&Index);
        VecGetValues(singProcessorImage,1,&Index,Values);
        PetscViewerASCIIPrintf(viewer, "%c",(unsigned char)(Values[0])); 
    }

    /* ================================================================ */ 
    /* Close Up Shop                                                    */
    /* ================================================================ */ 
 
    VecScatterDestroy(&scatterctx);
    VecDestroy(&singProcessorImage); 
    PetscViewerFlush(viewer);
    PetscViewerDestroy(&viewer);

}


