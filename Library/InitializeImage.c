#include "LibraryHeader.h"

fileInfo_t *readimage_init(int argc,char **args){
    
    /**
     *  This is a function to set the basic file and directory information
     *  for a given run of the image processing. It will make directories and 
     *  set defaults. Note it can not have a function call print for debugging 
     *  as it calls the PetscInitialize function. 
     */ 
    
    /* ====================================================================== */
    /* Do the PETSc initialize routine needed.                                */ 
    /* ====================================================================== */

    if((argc > 1) && (strcmp(args[1], "-parameter.file") == 0)){
        PetscInitialize(&argc,&args,args[2],0);
        PetscPrintf(PETSC_COMM_WORLD,"=========================================================\n");
        PetscPrintf(PETSC_COMM_WORLD,"Read in options from file %s \n",args[2]); 
        PetscPrintf(PETSC_COMM_WORLD,"=========================================================\n");
        PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);
    } else {
        PetscInitialize(&argc,&args,"image.params", 0);
        PetscPrintf(PETSC_COMM_WORLD,"=========================================================\n");
        PetscPrintf(PETSC_COMM_WORLD,"Read in options from default file image.params \n");
        PetscPrintf(PETSC_COMM_WORLD,"=========================================================\n");
        PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);
    }

    fileInfo_t *FileInfo;        /* File and directory to read and write from */ 
    PetscBool flag;              /* PETSC error flag that may be used.        */  

    FileInfo = (fileInfo_t *)malloc(sizeof(fileInfo_t));

    sprintf(FileInfo->saveBase[0], "%s", "./");
    
    /* ====================================================================== */
    /* Read in the options needed to set the directories and run parameters   */ 
    /* ====================================================================== */

    /* Document Code:-- optionfile-start --*/
    PetscOptionsGetString(PETSC_NULL,"-imageIn",
                          FileInfo->imageIn[0],PETSC_MAX_PATH_LEN-1,&flag);
    /* Document Code:-- optionfile-end --*/
    PetscOptionsGetString(PETSC_NULL,"-saveDirectory",FileInfo->saveBase[0],PETSC_MAX_PATH_LEN-1,&flag);
    PetscOptionsGetString(PETSC_NULL,"-readDirectory",FileInfo->readBase[0],PETSC_MAX_PATH_LEN-1,&flag); 
    PetscOptionsGetString(PETSC_NULL,"-imageOut",FileInfo->imageOut[0],PETSC_MAX_PATH_LEN-1,&flag);
    
    if(strcmp(FileInfo->saveBase[0], "./") != 0){ 
        mkdir(FileInfo->saveBase[0], S_IRWXU);
    }   

    return(FileInfo); 
}

image_t *Image_init(fileInfo_t *FileInfo){ 

    #ifdef DBG_FUNCTIONCALL
        PetscPrintf(MPI_COMM_WORLD,"CALL Image_init()\n");
        PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);
    #endif

    /** 
     * This is a function to initialize the needed an image from the given file 
     * information. There are examples of using both PETSc and MPI used below. 
     * The main read is done on the head process, and then synced with the others 
     * as needed.   
     */ 
    
    image_t *Image;                                      /* Place for the image          */
    char  NameOfFileToRead[PETSC_MAX_PATH_LEN];          /* Name of Input file name      */
    unsigned char value[1];                              /* Value for image pixel        */ 
    DMBoundaryType ptype = DM_BOUNDARY_PERIODIC;         /* Specify the periodicity      */
    DMDAStencilType  stype = DMDA_STENCIL_STAR;          /* stencil type for ghost nodes */
    PetscInt ghstOverLap = 1;                            /* Width of ghost nodes to keep */
    PetscInt TInumCols,TInumRows,TIbrightness;           /* Image Header Parameters      */
    PetscInt ppmImageSize;                               /* Image array spaces needed    */  
    PetscInt ImageType;                                  /* Type of image read from file */ 
    PetscInt i, Index;                                   /* Counting index for read      */  
    FILE *TIfp           = NULL;                         /* File pointer to image        */
    PetscMPIInt rank,size;                               /* Process rank and size        */
    PetscInt check;                                      /* Check Flag: MPI send/recv    */ 


    MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
    MPI_Comm_size(PETSC_COMM_WORLD,&size);

    PetscPrintf(PETSC_COMM_WORLD,"=========================================================\n");
    PetscPrintf(PETSC_COMM_WORLD,"Reading Image From: %s \n", &FileInfo->readBase);
    PetscPrintf(PETSC_COMM_WORLD,"   Image File Name: %s \n", &FileInfo->imageIn);
    PetscPrintf(PETSC_COMM_WORLD,"=========================================================\n"); 
    PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);

    /* ====================================================================== */
    /* Allocate the space needed.                                             */
    /* ====================================================================== */

    Image = (image_t *)malloc(sizeof(image_t));
    PetscOptionsGetInt(PETSC_NULL,"-processing",&Image->ProcType,PETSC_NULL); 

    /* ====================================================================== */
    /* Set the input file name.                                               */ 
    /* ====================================================================== */

    NameOfFileToRead[0] = 0;     
    strcat(NameOfFileToRead,FileInfo->readBase[0]); 
    strcat(NameOfFileToRead,FileInfo->imageIn[0]); 
    PetscPrintf(PETSC_COMM_WORLD,"Full Name: %s \n", NameOfFileToRead); 
    PetscPrintf(PETSC_COMM_WORLD,"=========================================================\n");
    PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);

    /* ====================================================================== */
    /* Read in the image parameters on a single processor.                    */
    /* ====================================================================== */   

    if(rank == 0){
        if ((TIfp=fopen(NameOfFileToRead, "r")) == NULL) {
            PetscPrintf(PETSC_COMM_WORLD,"Error Reading file : %s \n", NameOfFileToRead); 
            return(0);
        }
        /* ================================================================== */ 
        /* Read the header and get the type of image being read.              */
        /* ================================================================== */ 
        PetscPrintf(PETSC_COMM_WORLD,"Rank %d reading header. \n", rank);
        ImageType = parseHeader(TIfp, &TInumCols, &TInumRows, &TIbrightness);
        
    } else {
        PetscPrintf(PETSC_COMM_SELF,"Rank %d holding. \n",rank);
    }

    /* ====================================================================== */ 
    /* Communicate the header values for space of distributed image.          */ 
    /* ====================================================================== */ 

    if(rank == 0) {
        for (i=1;i<size;i++){
            check = MPI_Send(&TInumCols   , 1, MPI_INT, i, 0, MPI_COMM_WORLD);
            check = MPI_Send(&TInumRows   , 1, MPI_INT, i, 0, MPI_COMM_WORLD);
            check = MPI_Send(&ImageType   , 1, MPI_INT, i, 0, MPI_COMM_WORLD);
            check = MPI_Send(&TIbrightness, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
        }
    } else {
        check = MPI_Recv(&TInumCols   , 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        check = MPI_Recv(&TInumRows   , 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        check = MPI_Recv(&ImageType   , 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        check = MPI_Recv(&TIbrightness, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    /* ====================================================================== */ 
    /* Set degrees of freedom for pixels based on image type.                 */
    /* ====================================================================== */ 

    if(ImageType == 0){
        Image->DegreesOfFreedom = 1;
        PetscPrintf(PETSC_COMM_WORLD," Black and White Image Detected! ErrCheck %d \n",check); 
        PetscPrintf(PETSC_COMM_WORLD,"=========================================================\n");
    } else if (ImageType == 1){
        Image->DegreesOfFreedom = 3; 
        PetscPrintf(PETSC_COMM_WORLD," Color Image Detected! ErrCheck %d \n",check); 
        PetscPrintf(PETSC_COMM_WORLD,"=========================================================\n");
    } else {
        PetscPrintf(PETSC_COMM_WORLD, "Value of ImageType: %d \n", ImageType); 
        PetscPrintf(PETSC_COMM_WORLD, "Error: This is an incorrect ppm type. ErrCheck %d \n",check);
        PetscPrintf(PETSC_COMM_WORLD,"=========================================================\n");
        return(0);  
    }

    Image->ColumnsOld     = TInumCols; 
    Image->RowsOld        = TInumRows; 
    Image->BrightnessOld  = TIbrightness; 

    /* ================================================================= */ 
    /* Allocate Memory for the original image (type specific)            */
    /* ================================================================= */
    
    ppmImageSize = TInumRows * TInumCols * Image->DegreesOfFreedom;
    
    #ifdef DBG_BASIC
        PetscPrintf(PETSC_COMM_WORLD,"=DBG=====================================================\n");
        PetscPrintf(PETSC_COMM_WORLD," TInumCols      %i \n", TInumCols); 
        PetscPrintf(PETSC_COMM_WORLD," TInumRows      %i \n", TInumRows); 
        PetscPrintf(PETSC_COMM_WORLD," Image->ColumnsOld %i \n", Image->ColumnsOld); 
        PetscPrintf(PETSC_COMM_WORLD," Image->RowsOld    %i \n", Image->RowsOld); 
        PetscPrintf(PETSC_COMM_WORLD," Image->DegreesOfFreedom %i\n",Image->DegreesOfFreedom); 
        PetscPrintf(PETSC_COMM_WORLD,"=DBG_end=================================================\n");
        PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);
    #endif

    /* Document Code:-- DMDACreate-start --*/

    DMDACreate2d(PETSC_COMM_WORLD,ptype,ptype,stype,(PetscInt)Image->ColumnsOld,
                 (PetscInt)Image->RowsOld,PETSC_DECIDE,PETSC_DECIDE,
                 (PetscInt)Image->DegreesOfFreedom,ghstOverLap,
                 PETSC_NULL,PETSC_NULL, &Image->da_image);

    DMDAGetAO(Image->da_image,&Image->ao_image);
    DMDASetFieldName(Image->da_image,0,"red-pixel");
    DMDASetFieldName(Image->da_image,1,"green-pixel");
    DMDASetFieldName(Image->da_image,2,"blue-pixel");
    
    DMCreateGlobalVector(Image->da_image,&Image->pixelOld);

    if(rank == 0){
        for(i = 0 ; i < ppmImageSize ; i++){
            fread(value,1,1,TIfp); 
            Index = i; 
            AOApplicationToPetsc(Image->ao_image,1,&Index);
            #ifdef DBG_BASIC
                PetscPrintf(PETSC_COMM_WORLD,
                            "Char Observed: %i with %c \n",i, value[0]);
            #endif 
            VecSetValue(Image->pixelOld,Index,value[0],INSERT_VALUES);
        }
        fclose(TIfp); 
    }
    VecAssemblyBegin(Image->pixelOld);
    VecAssemblyEnd(Image->pixelOld);

    /* Document Code:-- DMDACreate-end --*/

    /* ================================================================= */ 
    /* A way of looking at how the data will be stored across processors */
    /* ================================================================= */  

    #ifdef DBG_DAVIEWERS
        PetscViewer    daviewer;                  
        PetscViewerDrawOpen(PETSC_COMM_WORLD,0,"",800,0,800,800,&daviewer);
        DMView(Image->da_image,daviewer);
        PetscViewerDestroy(&daviewer); 
    #endif
    #ifdef DBG_IMAGEREAD
       PetscViewer    viewer; 
       PetscViewerDrawOpen(PETSC_COMM_WORLD,NULL,NULL,0,0,300,300,&viewer);
       PetscObjectSetName((PetscObject)viewer,"Component Plot");
       PetscViewerPushFormat(viewer,PETSC_VIEWER_DRAW_LG);
       VecView(Image->pixelOld,viewer);
       PetscViewerDestroy(&viewer);
    #endif
    
    return(Image);
}

PetscInt parseHeader(FILE *inputFP, PetscInt *numColsPtr, PetscInt *numRowsPtr, PetscInt *brightnessPtr){

    #ifdef DBG_FUNCTIONCALL
        PetscPrintf(MPI_COMM_WORLD,"CALL parseHeader()\n");
    #endif

    /**
     * This function will parst a PPM file header. If sucessful it returns the
     * columns, rows, brightness, and 0 for P5 and 1 for P6. Note it is only 
     * called from the rank zero processor. 
     */ 

    PetscInt vals[3];                   /* converted values               */
    PetscInt hvseen = 0;                /* # of header values seen        */
    PetscInt count = 0;                 /* # of vals converted            */
    char fileType;                      /* PPM file type code             */
    char startCode;                     /* First byte of file             */
    char *buff = NULL;                  /* buffer to hold header comments */
    char comTest;		                /* Comment test                   */
    const char *typeNames[] = {"Grayscale","RGB"};
    
    /* ================================================================= */
    /* Allocate Memory to deal with comments.                            */ 
    /* ================================================================= */  

    PetscMalloc(1024,&buff);
    
    /* ================================================================= */
    /* Get the file type (first 2 bytes of file)                         */
    /* ================================================================= */  

    if (fscanf(inputFP, "%c %c", &startCode, &fileType) != 2) {
        PetscPrintf(MPI_COMM_WORLD,"Error: The file doesn't begin with the correct ppm code. \n");
        exit(1);
    }
    
    /* ================================================================= */
    /* Validate that this is a recognized PPM file type                  */
    /* ================================================================= */  

    if ((startCode != 'P') || ((fileType != '5') && (fileType != '6'))) {
        PetscPrintf(MPI_COMM_WORLD,"Error: The file is not a valid ppm file \n");
        exit(1);
    }

    /* ================================================================= */
    /* Input the rest of the header line                                 */
    /* ================================================================= */  

    while(hvseen <= 2 ){
       /* Read Info */
       if (hvseen == 0){
           count=fscanf(inputFP, "%d %d %d", &vals[hvseen], &vals[hvseen+1], &vals[hvseen+2]);
       }
       if (hvseen == 1){
           count=fscanf(inputFP, "%d %d", &vals[hvseen], &vals[hvseen+1]);
       }
       if (hvseen == 2){
           count=fscanf(inputFP, "%d", &vals[hvseen]);
       }
       /* Process Info */
       if (count == 0) {
           fgets(buff,1024, inputFP);
	       comTest = buff[0];
	       if  (comTest != '#'){
	           PetscPrintf(MPI_COMM_WORLD,"Error: The .ppm header has a nonvalid comment.\n");
	           exit(1);
	       }
           PetscPrintf(MPI_COMM_WORLD,"%s", buff);
       }
       if (count < 0 ) {
           PetscPrintf(MPI_COMM_WORLD,"Error: .ppm header not found for the given file.\n");
	       exit(1);
       }
       hvseen = hvseen + count; 
    }

    /* ================================================================= */  
    /* Print results From Parsing Header                                 */
    /* ================================================================= */  

    PetscPrintf(MPI_COMM_WORLD,"Got %d vals\n", hvseen);
    PetscPrintf(MPI_COMM_WORLD,"Type=%s, columns=%4d, rows=%4d, brightness=%4d\n",
             typeNames[fileType-'5'], vals[0], vals[1], vals[2]);

    /* ================================================================= */  
    /* Flush out rest of header line (position at start of image data)   */
    /* ================================================================= */  

    fgets(buff,1024,inputFP);                /* Get Rid of the '/n' Char */ 

    /* ================================================================= */        
    /* Check the Parsed Header infromation                               */ 
    /* ================================================================= */  

    if (vals[0] <= 0 ) {
        PetscPrintf(MPI_COMM_WORLD, "Error: The number of columns is negative. \n");
        exit(1);
    }
    if (vals[1] <= 0 ) {
        PetscPrintf(MPI_COMM_WORLD, "Error: The number of rows is negative. \n");
        exit(1);
    }

    /* ================================================================= */  
    /* Return values Needed.                                             */
    /* ================================================================= */  

    *numColsPtr = vals[0];
    *numRowsPtr = vals[1];
    *brightnessPtr = vals[2];
    
    /* ================================================================= */
    /* Returns a 0 if type P5 a 1 if type P6                             */
    /* ================================================================= */  

    return(fileType - '5');

}

