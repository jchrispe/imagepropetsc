#include "LibraryHeader.h"

void Process(image_t *Image){

    #ifdef DBG_FUNCTIONCALL
        PetscPrintf(MPI_COMM_WORLD,"CALL Process()\n");
        PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);
    #endif

    /** 
     * Function used to direct the image to the appropriate form of
     * manipulation. Users could add different functionality to the program here. 
     * The first type of processing added will be to manipulate a color image
     * into a black and white image version using a simple average of the pixel values.     
     */ 

     if(Image->ProcType == COLOR_TO_BW){
         ColorToBlackAndWhite_Average(Image);
     } else {
         PetscPrintf(MPI_COMM_WORLD,"====================================\n");
         PetscPrintf(MPI_COMM_WORLD,"Error:Image Processing Type Not Set!\n");
         PetscPrintf(MPI_COMM_WORLD,"====================================\n");
         PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);
     } 

}

void ColorToBlackAndWhite_Average(image_t *Image){

    #ifdef DBG_FUNCTIONCALL
        PetscPrintf(MPI_COMM_WORLD,"CALL ColorToBlackAndWhite_Average()\n");
        PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);
    #endif

    DMBoundaryType ptype = DM_BOUNDARY_PERIODIC;     /* Specify the periodicity       */
    DMDAStencilType  stype = DMDA_STENCIL_STAR;          /* stencil type for ghost pixels */
    PetscInt         ghstOverLap = 1;                    /* Width of ghost nodes to keep  */
    Vec              original_local;                     /* Storage Vector for pixels     */ 
    pixel_t          **old_pixel;                        /* Pixel array                   */
    unsigned char    value[1];                           /* Value for image pixel         */ 
    PetscInt         xptstart,yptstart;                  /* Starting Index local pixels   */ 
    PetscInt         xptstotal,yptstotal;                /* Local number of pixels total  */
    PetscInt         i,j,PixelIndex;                     /* Counting Index                */ 

    /* ================================================================ */ 
    /* Get the sizes on the local processor.                            */
    /* ================================================================ */

    DMDAGetCorners((DM)Image->da_image,&xptstart,&yptstart,PETSC_NULL,&xptstotal,&yptstotal,PETSC_NULL);

    /* ================================================================ */ 
    /* Set the size and create storage for the manipulated image.       */
    /* ================================================================ */ 

    Image->ColumnsNew          = Image->ColumnsOld; 
    Image->RowsNew             = Image->RowsOld; 
    Image->BrightnessNew       = Image->BrightnessOld; 
    Image->DegreesOfFreedomNew = 1;    

    #ifdef DBG_BASIC
        PetscPrintf(PETSC_COMM_WORLD,"=DBG=====================================================\n");
        PetscPrintf(PETSC_COMM_WORLD," Image->ColumnsNew %i \n", Image->ColumnsOld); 
        PetscPrintf(PETSC_COMM_WORLD," Image->RowsNew    %i \n", Image->RowsOld); 
        PetscPrintf(PETSC_COMM_WORLD," New Image DOF     %i \n", Image->DegreesOfFreedomNew); 
        PetscPrintf(PETSC_COMM_WORLD,"=DBG_end=================================================\n");
        PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);
    #endif

    DMDACreate2d(PETSC_COMM_WORLD,ptype,ptype,stype,(PetscInt)Image->ColumnsNew,(PetscInt)Image->RowsNew,PETSC_DECIDE,PETSC_DECIDE,1,ghstOverLap,PETSC_NULL,PETSC_NULL, &Image->da_imageNew); 
    DMDAGetAO(Image->da_imageNew,&Image->ao_imageNew);
    DMDASetFieldName(Image->da_imageNew,0,"pixel-value");
    DMCreateGlobalVector(Image->da_imageNew,&Image->pixelNew);

    #ifdef DBG_DAVIEWERS
        PetscViewer    daviewer;                  
        PetscViewerDrawOpen(PETSC_COMM_WORLD,0,"",800,0,800,800,&daviewer);
        DMView(Image->da_imageNew,daviewer);
        PetscViewerDestroy(&daviewer); 
    #endif

    /* ================================================================ */ 
    /* Get the local pixels for both original and manipulated image.    */
    /* ================================================================ */

    DMGetLocalVector(Image->da_image,&original_local);
    DMGlobalToLocalBegin(Image->da_image,Image->pixelOld,INSERT_VALUES,original_local);
    DMGlobalToLocalEnd(Image->da_image,Image->pixelOld,INSERT_VALUES,original_local);
    DMDAVecGetArray(Image->da_image,original_local,&old_pixel);

    /* ================================================================ */
    /* Loop over the pixels.                                            */
    /* ================================================================ */
    /* Document Code:-- color2bwporcess-start --*/
 
    for (j=yptstart; j<yptstart+yptstotal; j++) {
        for (i=xptstart; i<xptstart+xptstotal; i++){
            /* New Image Application Index to PETSc index */ 
            PixelIndex = j*Image->ColumnsOld + i; 
            AOApplicationToPetsc(Image->ao_imageNew,1,&PixelIndex);
            /* Find New Pixel Value */ 
            value[0] = ((PetscInt)(((PetscInt)old_pixel[j][i].r
                                  + (PetscInt)old_pixel[j][i].g
                                  + (PetscInt)old_pixel[j][i].b)/3.0));
            VecSetValue(Image->pixelNew,PixelIndex,value[0],INSERT_VALUES);
        }
    }

    /* Document Code:-- color2bwporcess-end --*/
    /* ================================================================ */ 
    /* Restore the pixels and clean up.                                 */
    /* ================================================================ */
    
    DMDAVecRestoreArray(Image->da_image,original_local,&old_pixel);

    VecAssemblyBegin(Image->pixelNew);
    VecAssemblyEnd(Image->pixelNew);

    #ifdef DBG_NEW_BW_IMAGE
       PetscViewer    viewer; 
       PetscViewerDrawOpen(PETSC_COMM_WORLD,NULL,NULL,0,0,300,300,&viewer);
       PetscObjectSetName((PetscObject)viewer,"Manipulated Image");
       PetscViewerPushFormat(viewer,PETSC_VIEWER_DRAW_LG);
       VecView(Image->pixelNew,viewer);
       PetscViewerDestroy(&viewer);
    #endif

}
