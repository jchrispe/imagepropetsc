#include "LibraryHeader.h"

/* This is a generic main used to test Library functions */ 
int main(int argc,char **args){
    PetscInitialize(&argc,&args,PETSC_NULL,PETSC_NULL);
    /* If ever there was a place for an easter egg this is it! */ 
    PetscPrintf(PETSC_COMM_WORLD,"Generic Main Sometimes used to test Library objects. \n");
    PetscPrintf(PETSC_COMM_WORLD,"Or an easter egg however you look at it. \n");
    PetscFinalize();
    return 0;
}
