#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <memory.h>
#include <assert.h>
#include <time.h>
#include <sys/stat.h>
#include "petscksp.h"
#include "petscdmda.h"

/* ===========================================================================*/
/* Constants used to make code more readable                                  */ 
/* ===========================================================================*/ 
#define COLOR_TO_BW  0

/* ===========================================================================*/ 
/* Data Structures                                                            */ 
/* ===========================================================================*/
/**
 *  @brief Structure used to pass around the files and directories used.     
 */ 
typedef struct File_and_Directory{
   char  paramFile[1][PETSC_MAX_PATH_LEN];        /* parameter file           */ 
   char  readBase[1][PETSC_MAX_PATH_LEN];         /* directory to look in     */ 
   char  saveBase[1][PETSC_MAX_PATH_LEN];         /* directory to save in     */
   char  imageIn[1][PETSC_MAX_PATH_LEN];          /* image file to read       */ 
   char  imageOut[1][PETSC_MAX_PATH_LEN];         /* name for output file     */
} fileInfo_t;

/**
 *  @brief Image_Struct is used to easily pass around the PETSc Da and pixel values.    
 */ 
typedef struct Image_Struct{
   PetscInt ProcType;            /** @param ProcType Type of processing to be done          */ 
   PetscInt DegreesOfFreedom;    /** @param DegreeOfFreedom (1->black and white, 3->color ) */ 
   PetscInt ColumnsOld;          /** @param ColumnsOld number of pixel columns in the image */ 
   PetscInt RowsOld;             /** @param RowsOld number of pixel rows in the image       */
   PetscInt BrightnessNew;       /** @param Maximum Image Brightness (original image)       */  
   PetscInt BrightnessOld;       /** @param Maximum Image Brightness (original image)       */  
   PetscInt ColumnsNew;          /** @param ColumnsNew number of pixel columns in the image */ 
   PetscInt RowsNew;             /** @param RowsNew number of pixel rows in the image       */ 
   PetscInt DegreesOfFreedomNew; /** @param DegreeOfFreedom (1->black and white, 3->color ) */ 
   DM       da_image;            /** @param da_image PETSc distributed array for the image  */ 
   AO       ao_image;            /** @param ao_image PETSc Application Ordering for image   */  
   DM       da_imageNew;         /** @param da_image distributed array for the new   image  */ 
   AO       ao_imageNew;         /** @param ao_image Application Ordering for new image     */  
   Vec      pixelOld;            /** @param pixel array used for original image             */
   Vec      pixelNew;            /** @param pixel array used for new image                  */ 
} image_t;

/**
 *  @brief Structure used to easily deal with each pixel.     
 */ 
typedef struct Pixel{
   PetscScalar r;         /** @param r red pixel   */   
   PetscScalar g;         /** @param g green pixel */ 
   PetscScalar b;         /** @param b blue pixel  */ 
} pixel_t;

/* ===========================================================================*/ 
/* Functions                                                                  */ 
/* ===========================================================================*/
/* InitializeImages.c                                                         */
/* ---------------------------------------------------------------------------*/ 
fileInfo_t *readimage_init(int argc,char **args);
image_t *Image_init(fileInfo_t *FileInfo); 
PetscInt parseHeader(FILE *inputFP, PetscInt *numColsPtr, PetscInt *numRowsPtr, PetscInt *brightnessPtr);
/* WriteImage.c                                                               */ 
/* ---------------------------------------------------------------------------*/
void Image_Write(fileInfo_t *FileInfo, image_t *Image); 
/* ManipulateImage.c                                                          */ 
/* ---------------------------------------------------------------------------*/
void Process(image_t *Image); 
void ColorToBlackAndWhite_Average(image_t *Image); 

