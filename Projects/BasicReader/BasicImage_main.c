#include "../../Library/LibraryHeader.h"

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **args){

    /* =========================================================================*/ 
    /* All the needed variables                                                 */ 
    /* =========================================================================*/ 

    fileInfo_t     *FileInfo;                    /* Files/Directory info needed */ 
    image_t        *Image;                       /* The Image Structure used    */
    clock_t        ClockStart, ClockEnd;         /* Stop watch for CPU time     */ 
    double         WClockStart, WClockEnd;       /* Wall Clock Start and Stop   */

    /* =========================================================================*/ 
    /* Initalize MPI and set things so the code will run in parallel            */ 
    /* =========================================================================*/

    FileInfo = readimage_init(argc,args);
    PetscPrintf(PETSC_COMM_WORLD,
    "Usage: /Set/Path/To/petscmpiexec -n <number> ./ImagePro \"Runtime options \"\n");
    PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);

    /* =========================================================================*/
    /* Start the Stop Watch                                                     */
    /* =========================================================================*/

    WClockStart = MPI_Wtime();
    
    /* =========================================================================*/ 
    /* Initalize: Read in the image to manipulate.                              */ 
    /* =========================================================================*/ 

    Image = Image_init(FileInfo); 

    /* =========================================================================*/ 
    /* Process: Read in the image to manipulate.                                */ 
    /* =========================================================================*/ 

    ClockStart  = clock();
    Process(Image);
    ClockEnd    = clock();
    
    WClockEnd   = MPI_Wtime();;
  
    /* =========================================================================*/ 
    /* Write: Put the manipulated image out to a file.                          */ 
    /* =========================================================================*/ 

    Image_Write(FileInfo,Image);

    /* =========================================================================*/ 
    /*                         REPORT TIMES USED                                */ 
    /* =========================================================================*/

    PetscPrintf(PETSC_COMM_WORLD,"Time used for image processing: \n");
    PetscPrintf(PETSC_COMM_WORLD,"   Run  Time = %f \n", (ClockEnd-ClockStart)/(double)CLOCKS_PER_SEC); 
    PetscPrintf(PETSC_COMM_WORLD,"   Wall Time = %f \n", WClockEnd-WClockStart);
    PetscSynchronizedFlush(PETSC_COMM_WORLD,PETSC_STDOUT);

    /* =========================================================================*/ 
    /*                         CLEAN UP AND EXIT                                */ 
    /* =========================================================================*/

    PetscFinalize();
    return 0;

} 
   
